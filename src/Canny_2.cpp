#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


int const max_value = 255;
int const max_type = 4;
int const max_BINARY_value = 255;
int canny_low=30;
int ratio=3;
int kernellsize=3;

using namespace cv;
using namespace std;

 int main(int argc, char** argv)
 {
	ros::init(argc, argv, "Canny_2");
	ros::NodeHandle n;
	ros::Rate loop_rate(10);

	namedWindow("Control", CV_WINDOW_AUTOSIZE);
	createTrackbar("Canny Low Bound", "Control", &canny_low, 80);
	
	int c;
	Mat input_image,grey_image,threshold_image,edges_image;
    VideoCapture capture;
	
	capture.open(0);	
	//publisher
	image_transport::ImageTransport it(n);
  	image_transport::Publisher image_pub_ = it.advertise("/edges_image", 1);
	waitKey(30);
	
	 if(capture.isOpened())
    {
        ROS_INFO("CAMERA OPENED");

	
	while(ros::ok()){
		capture >> input_image;
            if(input_image.empty())
                break;
		
		cvtColor(input_image,grey_image,CV_BGR2GRAY);
		//Apply Canny detector
		Canny(grey_image,edges_image,canny_low,ratio*canny_low,kernellsize);
		imshow("edges_image",edges_image);
		c=waitKey(50);
		if(c == 27)
             		 break;
		//Transfrom from OpenCv to ROS & publish
		sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "mono8", edges_image).toImageMsg();
		image_pub_.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	 }
	}
	 else
    {
        cout << "No capture" << endl;
        input_image = Mat::zeros(480, 640, CV_8UC1);
        imshow("Sample", input_image);
        waitKey(0);
		ros::spinOnce();
    }
 	
   return 0;
 }

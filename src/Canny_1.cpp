#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


int const max_value = 80;
int canny_low=30;
int ratio=3;
int kernellsize=3;

using namespace cv;

    
  class ImageConverter
   {
     ros::NodeHandle n;
     image_transport::ImageTransport it;
     image_transport::Subscriber image_sub_;
     image_transport::Publisher image_pub_;
   
   public:
    ImageConverter()
     : it(n)
   {
   
     image_sub_ = it.subscribe("/camera/image_raw", 1,
       &ImageConverter::imageCb, this);
     image_pub_ = it.advertise("/canny_image", 1);
  
     namedWindow("Output Image");
   }
 
   ~ImageConverter()
   {
     destroyWindow("Output Image");
   }
 
   void imageCb(const sensor_msgs::ImageConstPtr& msg)
   {
     cv_bridge::CvImagePtr cv_ptr;
	
     try
     {
       cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
     }
     catch (cv_bridge::Exception& e)
     {
       ROS_ERROR("cv_bridge exception: %s", e.what());
     }
   
	//Apply Canny detector
	Canny(cv_ptr->image,cv_ptr->image,canny_low,ratio*canny_low,kernellsize);

 	
     // Update GUI Window
     imshow("Output Image", cv_ptr->image);
    waitKey(10);
	
 
     // Output modified video stream
     image_pub_.publish(cv_ptr->toImageMsg());
    }
  };
 
 int main(int argc, char** argv)
 {
	ros::init(argc, argv, "Canny_1");
	namedWindow("Control", CV_WINDOW_AUTOSIZE);
	createTrackbar("Canny Low Bound", "Control", &canny_low, max_value);
	ImageConverter ic;
	ROS_INFO("spinnin");
	ros::spin();
	return 0;
 }
